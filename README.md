<!-- # [<img src="https://lh3.googleusercontent.com/a-/AOh14Gilqms4ucFuPLLOrS93UYMNXtYGtDXxeDPqrHDNFA=s96-c-rg-br100" width="40" height="40">](https://github.com/sarveshspatil111) Hi, I’m Sarvesh Patil -->

<h1>Hello, I'm Sarvesh Patil</h1>

<a href='https://www.linkedin.com/in/sarveshspatil/' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=sarveshspatil&color=gray&style=plastic-square&logo=linkedin' />
  </a>
<a href='https://leetcode.com/sarveshsp/' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=sarveshsp&color=gray&style=plastic-square&logo=leetcode' />
  </a>
<a href='https://www.youtube.com/channel/UCMQBfRsIP5RPRcD_j9hHmYw' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=SarveshPatil&color=gray&style=plastic-square&logo=youtube' />
  </a>
<a href='https://www.datacamp.com/profile/sarveshsp' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=sarveshsp&color=gray&style=plastic-square&logo=datacamp' />
  </a>
<a href='https://twitter.com/sarveshspatil' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=sarveshspatil&color=gray&style=plastic-square&logo=twitter' />
  </a>
<a href='https://img.shields.io/static/v1?label=&message=sarveshspatil111@gmail.com&color=gray&style=plastic-square&logo=gmail' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=sarveshspatil111@gmail.com&color=gray&style=plastic-square&logo=gmail' />
  </a>
<a href='https://drive.google.com/file/d/1bFHBe8Huapz-uzVnd3SVGqOvrWre8Tsn/view?usp=sharing' target='_blank' rel='noopener' rel='noreferrer'>
    <img src='https://img.shields.io/static/v1?label=&message=Resume&color=gray&style=plastic-square&logo=files' />
  </a>

```fish
><> fetchinfo
```
<img align="left" src="https://raw.githubusercontent.com/sarveshspatil111/sarveshspatil111/main/assets/endeavouros.png" width="265" />

```csharp
sarveshspatil111@github
-------------------------
OS: EndeavourOS Linux x86_64
Shell: fish 3.3.1
WM: sway
Terminal: alacritty
Languages: Python, JavaScript, HTML5, CSS3, SQL, C
Frameworks: Django, React
Learning: Node.js, Express, Frappe
Tools: Git, Docker, Postman
```

<!-- ![](https://github-readme-streak-stats.herokuapp.com?user=sarveshspatil111&theme=tokyonight&hide_border=true) -->
